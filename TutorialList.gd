extends ItemList


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var tutorial_list = [
["How to import a shader from ShaderToy to Godot","res://icons/how_to_import_ShaderToy.png","res://tutorials/how_to_import_ShaderToy/how_to_import_ShaderToy.tscn"
],
["How to use containers","res://icons/how_to_use_containers.png","res://tutorials/how_to_use_containers/how_to_use_containers.tscn"],
["How to create a procedural mesh with ArrayMesh","res://icons/how_to_create_procedural_mesh_with_ArrayMesh.png","res://tutorials/how_to_create_procedural_mesh_with_ArrayMesh/how_to_create_procedural_mesh_with_ArrayMesh.tscn"]]
var selected_items
# Called when the node enters the scene tree for the first time.
func _ready():
	for entry_index in range(len(tutorial_list)):
		add_item(tutorial_list[entry_index][0],  load(tutorial_list[entry_index][1]))
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	if is_anything_selected():
		selected_items = get_selected_items()
		get_tree().change_scene(tutorial_list[selected_items[0]][2])
	pass # Replace with function body.
