extends MeshInstance

var rings = 50
var radial_segments = 50
var height = 1
var radius = 1

var generate_mesh = true
var surface_count = 0

export(String,"mesh array", "surface tool", "misc") var mesh_generation_type 

func generate_box_mesh_array():
	var arr = []
	arr.resize(Mesh.ARRAY_MAX)
	
	var verts = PoolVector3Array()
	var uvs = PoolVector2Array()
	var normals = PoolVector3Array()
	var indices = PoolIntArray()
	var vert
	
	vert=Vector3(1.0,1.0,1.0)
	verts.append(vert)
	normals.append(vert.normalized())
	
	# Assign arrays to mesh array.
	arr[Mesh.ARRAY_VERTEX] = verts
	arr[Mesh.ARRAY_TEX_UV] = uvs
	arr[Mesh.ARRAY_NORMAL] = normals
	arr[Mesh.ARRAY_INDEX] = indices

	# Create mesh surface from mesh array.
	mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arr) # No blendshapes or compression used.
	pass

func generate_sphere_mesh_array():
	var arr = []
	arr.resize(Mesh.ARRAY_MAX)

	# PoolVectorXXArrays for mesh construction.
	var verts = PoolVector3Array()
	var uvs = PoolVector2Array()
	var normals = PoolVector3Array()
	var indices = PoolIntArray()

	# Vertex indices.
	var thisrow = 0
	var prevrow = 0
	var point = 0

	# Loop over rings.
	for i in range(rings + 1):
		var v = float(i) / rings
		var w = sin(PI * v)
		var y = cos(PI * v)

		# Loop over segments in ring.
		for j in range(radial_segments):
			var u = float(j) / radial_segments
			var x = sin(u * PI * 2.0)
			var z = cos(u * PI * 2.0)
			var vert = Vector3(x * radius * w, y, z * radius * w)
			verts.append(vert)
			normals.append(vert.normalized())
			uvs.append(Vector2(u, v))
			point += 1

			# Create triangles in ring using indices.
			if i > 0 and j > 0:
				indices.append(prevrow + j - 1)
				indices.append(prevrow + j)
				indices.append(thisrow + j - 1)

				indices.append(prevrow + j)
				indices.append(thisrow + j)
				indices.append(thisrow + j - 1)

		if i > 0:
			indices.append(prevrow + radial_segments - 1)
			indices.append(prevrow)
			indices.append(thisrow + radial_segments - 1)

			indices.append(prevrow)
			indices.append(prevrow + radial_segments)
			indices.append(thisrow + radial_segments - 1)

		prevrow = thisrow
		thisrow = point

	# Assign arrays to mesh array.
	arr[Mesh.ARRAY_VERTEX] = verts
	arr[Mesh.ARRAY_TEX_UV] = uvs
	arr[Mesh.ARRAY_NORMAL] = normals
	arr[Mesh.ARRAY_INDEX] = indices

	# Create mesh surface from mesh array.
	mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arr) # No blendshapes or compression used.
	print("vertex count: ", str(verts.size()))

func _ready():
	if mesh_generation_type == "mesh array":
		generate_sphere_mesh_array()
		surface_count = 1
		generate_mesh = false
	
# func _process(delta):

func _on_ShadingOptionButton_item_selected(index):
	var vp = get_viewport()
	if index == 1:
		VisualServer.set_debug_generate_wireframes(true)
		vp.debug_draw = Viewport.DEBUG_DRAW_WIREFRAME
		generate_mesh = true
	if index == 0:
		VisualServer.set_debug_generate_wireframes(false)
		generate_mesh = true

func _on_RadiusSpinBox_value_changed(value):
	radius = value
	
func _on_HeightSpinBox_value_changed(value):
	height = value

func _on_RingsSpinBox_value_changed(value):
	rings = value
	
func _on_SegmentsSpinBox_value_changed(value):
	radial_segments = value
	
func _on_GenerateButton_pressed():
	generate_mesh = true
	if mesh_generation_type == "mesh array" and generate_mesh:
		
		if surface_count > 0:
			for x in range(surface_count):
				mesh.surface_remove(0)
		generate_sphere_mesh_array()
		generate_mesh = false
		surface_count = mesh.get_surface_count()

